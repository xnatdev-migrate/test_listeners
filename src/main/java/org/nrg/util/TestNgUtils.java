package org.nrg.util;

import org.testng.ITestNGMethod;
import org.testng.ITestResult;

public class TestNgUtils {

    public static String getTestName(ITestResult result) {
        return result.getMethod().getMethodName();
    }

    public static Class getTestClass(ITestResult result) {
        return result.getMethod().getRealClass();
    }

    public static String getTestClassName(ITestResult result) {
        return getTestClass(result).getSimpleName();
    }

    public static String getTestName(ITestNGMethod method) {
        return method.getMethodName();
    }

    public static Class getTestClass(ITestNGMethod method) {
        return method.getRealClass();
    }

    public static String getTestClassName(ITestNGMethod method) {
        return getTestClass(method).getSimpleName();
    }

    public static boolean testFailed(ITestResult result) {
        return result.getStatus() == ITestResult.FAILURE;
    }
}