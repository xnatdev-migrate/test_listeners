package org.nrg.util;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AssortedUtils {

    private static final SimpleDateFormat TIMESTAMP_FORMATTER = new SimpleDateFormat("yyyyMMdd_HHmmss");

    public static String getTimestamp() {
        return TIMESTAMP_FORMATTER.format(new Date());
    }

    public static String formatUrl(String... components) {
        String[] trimmedComponents = StringUtils.stripAll(components, "/");

        return StringUtils.join(trimmedComponents, "/");
    }
}
