package org.nrg.listeners.git;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.listeners.BaseListener;
import org.nrg.util.TestNgUtils;
import org.testng.ITestResult;

public class GitLogListener extends BaseListener {

    private static String USERNAME, PASSWORD, XNAT_URL;
    private static final Logger LOGGER = Logger.getLogger(GitLogListener.class);
    private static boolean initialized = false;

    public static void init(String username, String password, String xnatUrl) {
        USERNAME = username;
        PASSWORD = password;
        XNAT_URL = xnatUrl;
        final Response response = credentials().post(formatUrl(XNAT_URL, "xapi", "testlog", "init"));
        if (response.getStatusCode() == 200) {
            initialized = true;
        } else {
            LOGGER.warn("Could not initialize git repos from XNAT logs. Status code: " + response.getStatusCode());
        }
    }

    @Override
    public void onTestComplete(ITestResult testResult) {
        if (initialized) {
            final Response response = credentials().post(formatUrl(XNAT_URL, "xapi", "testlog", "commit", TestNgUtils.getTestName(testResult)));
            if (response.getStatusCode() != 200) {
                LOGGER.warn("Could not commit changes to XNAT log git repos. Status code: " + response.getStatusCode());
            }
        }
    }

    private static RequestSpecification credentials() {
        return RestAssured.given().authentication().preemptive().basic(USERNAME, PASSWORD);
    }

    private static String formatUrl(String... components) {
        String[] trimmedComponents = StringUtils.stripAll(components, "/");

        return StringUtils.join(trimmedComponents, "/");
    }

}
