package org.nrg.listeners.jira.failure;

public class FailureExplanation implements FailureCause {

    private String reason;

    public FailureExplanation(String reason) {
        this.reason = reason;
    }

    @Override
    public String getHTMLReason() {
        return reason;
    }

}
