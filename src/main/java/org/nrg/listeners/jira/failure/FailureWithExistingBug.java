package org.nrg.listeners.jira.failure;

import org.nrg.jira.JiraManager;

public class FailureWithExistingBug implements FailureCause {

    private String reason;
    private String bug;
    private JiraManager jiraManager;

    public FailureWithExistingBug(String reason, String bug, JiraManager jiraManager) {
        this.reason = reason;
        this.bug = bug;
        this.jiraManager = jiraManager;
    }

    @Override
    public String getHTMLReason() {
        return String.format("%s. See: %s", reason, getBugLink());
    }

    private String getBugLink() {
        return (jiraManager == null) ? bug : String.format("<a href=\"%s\">%s</a>", jiraManager.formatJiraUrl("browse", bug), bug);
    }

}
