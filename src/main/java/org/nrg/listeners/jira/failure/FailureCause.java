package org.nrg.listeners.jira.failure;

public interface FailureCause {

    public String getHTMLReason();

}
