/*
 * org.nrg.selenium.listeners.TestListener
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.listeners.jira;

import org.apache.log4j.Logger;
import org.nrg.jira.JiraManager;
import org.nrg.jira.exceptions.JiraZephyrException;
import org.nrg.jira.testing_components.Cycle;
import org.nrg.jira.testing_components.TestStatus;
import org.nrg.listeners.BaseListener;
import org.nrg.util.AssortedUtils;
import org.nrg.util.TestNgUtils;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JIRATestNGListener extends BaseListener {

    private static final Logger LOGGER = Logger.getLogger(JIRATestNGListener.class);
    protected String currentTestNumber = null;
    protected JIRATest currentTest = null;
    protected static Cycle cycle;
    private static JiraManager jiraManager;
    private static final Map<ITestNGMethod, JIRATest> jiraTests = new HashMap<>();

    @Override
    public void onTestStart(ITestResult result)  {
        super.onTestStart(result);
        getCurrentTest(result.getMethod()).updateExecutionStatus(TestStatus.WIP);
        LOGGER.info("Starting test: " + testName);
    }

    @Override
    public void onTestFailure(ITestResult testResult)  {
        super.onTestFailure(testResult);

        currentTest.failTest();

        if (currentTestNumber != null) {
            LOGGER.warn(String.format("%s (%s) failed!", testName, currentTestNumber));
        }
        else {
            LOGGER.warn(testName + " failed!");
        }
    }

    @Override
    public void onTestSuccess(ITestResult testResult) {
        super.onTestSuccess(testResult);

        currentTest.passTest();

        if (currentTestNumber != null) {
            LOGGER.info(String.format("Test passed: %s (%s)", testName, currentTestNumber));
        } else {
            LOGGER.info("Test passed: " + testName);
        }
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        super.onTestSkipped(testResult);

        currentTest.skipTest();

        if (currentTestNumber != null) {
            LOGGER.info(String.format("Test skipped: %s (%s)", testName, currentTestNumber));
        } else {
            LOGGER.info("Test skipped: " + testName);
        }
    }

    @Override
    public void onTestComplete(ITestResult testResult) {
        currentTest = getCurrentTest(testResult.getMethod());
        currentTestNumber = currentTest.getJiraNumber();
    }

    /**
     * Creates cycle and initializes JIRA tests
     * @param jiraUrl URL for your JIRA
     * @param jiraProject Project that contains the tests
     * @param fixVersion Fix version to associate tests to
     * @param cycleName Name of the cycle to create
     * @param jiraUser Username for authentication
     * @param jiraPassword Password for authentication
     * @param allRunningTests Map of test objects to their corresponding JIRA number (e.g. XNAT-101). All tests <b>MUST</b> be included in this list, but if no JIRA test is available, the test object can be mapped to null
     */
    public static void init(String jiraUrl, String jiraProject, String fixVersion, String cycleName, String jiraUser, String jiraPassword, Map<ITestNGMethod, String> allRunningTests) {
        createCycle(jiraUrl, jiraProject, fixVersion, cycleName, jiraUser, jiraPassword);
        readJIRATests(allRunningTests);
    }

    public static void createCycle(String jiraUrl, String jiraProject, String fixVersion, String cycleName, String jiraUser, String jiraPassword) {
        cycleName += ": " + AssortedUtils.getTimestamp();
        try {
            jiraManager = new JiraManager(jiraUrl, jiraUser, jiraPassword);
            cycle = jiraManager.createCycle(cycleName, jiraProject, fixVersion);
        } catch (JiraZephyrException e) {
            LOGGER.warn("Error occurred in creating cycle in JIRA.", e);
        }
    }

    public static void readJIRATests(List<ITestNGMethod> allRunningTests) {
        final Map<ITestNGMethod, String> boringMap = new HashMap<>();

        for (ITestNGMethod method : allRunningTests) {
            boringMap.put(method, null);
        }

        readJIRATests(boringMap);
    }

    /**
     * Reads in map to populate JIRA test objects
     * @param testMap Map from ITestNGMethod objects to their JIRA test number (e.g. XNAT-101). Any test to be run without an ID should still be included in the map (mapped to null).
     */
    public static void readJIRATests(Map<ITestNGMethod, String> testMap) {
        jiraTests.clear();

        for (Map.Entry<ITestNGMethod, String> testEntry : testMap.entrySet()) {
            final ITestNGMethod test = testEntry.getKey();
            final String testNumber = testEntry.getValue();
            final String testName = TestNgUtils.getTestName(test);
            final String testClass = TestNgUtils.getTestClassName(test);

            if (cycle != null && testNumber != null) { // If setup has been called (JIRA testing is go) and test has a JIRA test
                JIRATest jiraTest = new JIRATest(jiraManager, cycle, testName, testNumber, testClass);
                jiraTest.createExecution();
                jiraTests.put(test, jiraTest);
            } else { // if JIRA integration is on, handle those tests which didn't have a test ID. Otherwise, handle everything as noninteractive tests
                jiraTests.put(test, new NonInteractiveTest(testName, testClass));
            }
        }
    }

    public static JIRATest getCurrentTest(ITestNGMethod test) {
        return jiraTests.get(test);
    }

    public static void updateEnvironmentInfo(String environment) {
        try {
            jiraManager.updateEnvironmentInfo(cycle, environment);
        } catch (Exception e) {
            LOGGER.warn("Could not update environment information due to:", e);
        }
    }

    public static void updateBuildInfo(String build) {
        try {
            jiraManager.updateBuildInfo(cycle, build);
        } catch (Exception e) {
            LOGGER.warn("Could not update build information due to:", e);
        }
    }

    public static Cycle getCycle() {
        return cycle;
    }

}