package org.nrg.listeners.jira;

import org.apache.log4j.Logger;
import org.junit.AssumptionViolatedException;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nrg.jira.JiraManager;
import org.nrg.jira.exceptions.JiraZephyrException;
import org.nrg.jira.testing_components.Cycle;
import org.nrg.jira.testing_components.TestStatus;
import org.nrg.util.AssortedUtils;

import java.util.HashMap;
import java.util.Map;

public class JIRAJunitListener extends TestWatcher {

    private static boolean inited = false;
    private static final Logger logger = Logger.getLogger(JIRAJunitListener.class);
    private static JiraManager jiraManager = null;
    private static Cycle cycle = null;
    private static JIRATest currentTest = null;
    private static final Map<String, String> testMap = new HashMap<>(); // maps test name to JIRA test number

    private static void createCycle(String jiraUrl, String jiraProject, String fixVersion, String cycleName, String jiraUser, String jiraPassword) {
        try {
            jiraManager = new JiraManager(jiraUrl, jiraUser, jiraPassword);
            cycle = jiraManager.createCycle(cycleName, jiraProject, fixVersion);
        } catch (JiraZephyrException e) {
            logger.warn("Error occurred in creating cycle in JIRA.", e);
        }
    }

    public void init(String jiraUrl, String jiraProject, String fixVersion, String cycleName, String jiraUser, String jiraPassword, Map<String, String> tests) {
        if (inited) return;
        cycleName += ": " + AssortedUtils.getTimestamp();
        createCycle(jiraUrl, jiraProject, fixVersion, cycleName, jiraUser, jiraPassword);
        if (tests != null && !tests.isEmpty()) {
            testMap.putAll(tests);
        }
        inited = true;
    }

    @Override
    protected void starting(Description description) {
        if (cycle == null) return;
        final String testName = description.getMethodName();
        final Class testClass = description.getTestClass();
        if (testMap.containsKey(testName)) {
            final JIRATest jiraTest = new JIRATest(jiraManager, cycle, testName, testMap.get(testName), testClass.getSimpleName());
            jiraTest.createExecution();
            jiraTest.updateExecutionStatus(TestStatus.WIP);
            currentTest = jiraTest;
        }
    }

    @Override
    protected void failed(Throwable e, Description description) {
        if (cycle == null || currentTest == null) return;

        currentTest.failTest();
        currentTest = null;
    }

    @Override
    protected void succeeded(Description description) {
        if (cycle == null || currentTest == null) return;

        currentTest.passTest();
        currentTest = null;
    }

    @Override
    protected void skipped(AssumptionViolatedException e, Description description) {
        if (cycle == null || currentTest == null) return;

        currentTest.skipTest();
        currentTest = null;
    }

}
