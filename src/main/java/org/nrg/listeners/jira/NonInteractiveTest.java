package org.nrg.listeners.jira;

import org.apache.log4j.Logger;
import org.nrg.jira.testing_components.TestStatus;

import java.io.File;

public class NonInteractiveTest extends JIRATest {

    private static final Logger LOGGER = Logger.getLogger(NonInteractiveTest.class);

    public NonInteractiveTest(String testName, String methodClass) {
        super(null, null, testName, null, methodClass);
    }

    @Override
    public String getExecutionUrl() {
        return null;
    }

    @Override
    public boolean hasSteps() {
        return false;
    }

    @Override
    public String getJiraNumber() {
        return null;
    }

    @Override
    public void createExecution() {}

    @Override
    public void updateExecutionStatus(TestStatus status) {}

    @Override
    public void postExecutionComment(String comment) {
        if (comment == null) return;
        LOGGER.info(String.format("Test %s requested the following comment to be posted to JIRA: %s", testName, comment));
    }

    @Override
    public void postDefect(String issue) {
        if (issue == null) return;
        LOGGER.info(String.format("Test %s attempted to link to defect in JIRA: %s", testName, issue));
    }

    @Override
    public void postExecutionAttachment(File attachment) {}

    @Override
    public void postStepAttachment(File attachment, int step) {}

    @Override
    public void postStepAttachment(File attachment) {}

    @Override
    public void updateStepResult(int step, TestStatus status, String comment) {
        postExecutionComment(comment);
    }

    @Override
    public void failTest() {}

    @Override
    public void passTest() {}

    @Override
    public void skipTest() {}

}
