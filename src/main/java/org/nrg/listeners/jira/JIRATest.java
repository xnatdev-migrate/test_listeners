package org.nrg.listeners.jira;

import org.apache.log4j.Logger;
import org.nrg.jira.JiraManager;
import org.nrg.jira.testing_components.Cycle;
import org.nrg.jira.testing_components.Execution;
import org.nrg.jira.testing_components.TestStatus;
import org.nrg.jira.testing_components.TestStep;
import org.nrg.jira.exceptions.AttachmentNotFound;
import org.nrg.jira.exceptions.JiraZephyrException;
import org.nrg.listeners.jira.failure.FailureCause;
import org.nrg.listeners.jira.failure.FailureExplanation;
import org.nrg.listeners.jira.failure.FailureWithExistingBug;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JIRATest {

    protected JiraManager jiraManager;
    protected Cycle cycle;
    protected Execution execution;
    protected List<TestStep> testSteps = new ArrayList<>();
    protected String testName;
    protected String jiraNumber;
    protected String methodClass;
    protected int lastSuccessfulStep = 0;
    protected static final Logger LOGGER = Logger.getLogger(JIRATest.class);
    protected FailureCause failureReason;
    protected static boolean stepAttachmentsAllowed = true;

    public JIRATest(JiraManager jiraManager, Cycle cycle, String testName, String jiraNumber, String methodClass) {
        this.jiraManager = jiraManager;
        this.cycle = cycle;
        this.testName = testName;
        this.jiraNumber = jiraNumber;
        this.methodClass = methodClass;
    }

    public static void disableStepAttachments() {
        stepAttachmentsAllowed = false;
    }

    public String getExecutionUrl() {
        String query = "?query=cycleName%3D%22" + cycle.getName().replace(" ", "%20").replace(":", "%3A") + "%22";
        return jiraManager.formatJiraUrl("secure", "enav", "#", execution.getId() + query);
    }

    public boolean hasSteps() {
        return !testSteps.isEmpty();
    }

    public String getJiraNumber() {
        return jiraNumber;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = new FailureExplanation(failureReason);
    }

    public void setFailureReason(String failureReason, String jiraBugNumber) {
        this.failureReason = new FailureWithExistingBug(failureReason, jiraBugNumber, jiraManager);
        postDefect(jiraBugNumber);
    }

    public FailureCause getFailureReason() {
        return failureReason;
    }

    public void createExecution() {
        try {
            execution = jiraManager.createExecution(cycle, jiraNumber);
            testSteps = execution.getTestSteps();
        } catch (Exception e) {
            LOGGER.warn("Failed to create execution for test: " + jiraNumber, e);
        }
    }

    public void updateExecutionStatus(TestStatus status) {
        try {
            jiraManager.updateExecutionStatus(execution, status);
        } catch (Exception e) {
            LOGGER.warn("Error occurred when updating execution.", e);
        }
    }

    public void postExecutionComment(String comment) {
        try {
            jiraManager.postExecutionComment(execution, comment);
        } catch (Exception e) {
            LOGGER.warn("Error occurred when posting execution comment.", e);
        }
    }

    public void postDefect(String defect) {
        try {
            jiraManager.postDefect(execution, defect);
        } catch (Exception e) {
            LOGGER.warn("Error occurred when posting defect.", e);
        }
    }

    public void postExecutionAttachment(File attachment) {
        try {
            jiraManager.postExecutionAttachment(execution, attachment);
        } catch (AttachmentNotFound e) {
            LOGGER.warn(String.format("Attachment %s not found to post to JIRA.", attachment), e);
        } catch (Exception e) {
            LOGGER.warn("Unknown error when posting attachment to JIRA.");
        }
    }

    public void postStepAttachment(File attachment, int step) {
        if (!stepAttachmentsAllowed) {
            LOGGER.debug("Step attachments are not allowed. Skipping posting of " + attachment);
            return;
        }
        try {
            jiraManager.postStepAttachment(getNaturalStep(step), attachment);
        } catch (IndexOutOfBoundsException e) {
            LOGGER.warn(String.format("Step %d not found for test %s", step, testName));
        } catch (Exception e) {
            LOGGER.warn("Unknown error when posting step attachment to JIRA.");
        }
    }

    public void postStepAttachment(File attachment) {
        postStepAttachment(attachment, lastSuccessfulStep + 1);
    }

    /**
     * Updates the step in JIRA
     * @param step     Index for the step, numbered <i>naturally</i> (e.g. starting with 1)
     * @param status   Status for the step
     * @param comment  Comment for the step
     */
    public void updateStepResult(int step, TestStatus status, String comment) {
        if (!hasSteps()) return;
        TestStep executionStep;
        try {
            executionStep = getNaturalStep(step); // natural ordering of test steps 1, 2, 3, ... to match what is returned by ZAPI
        } catch (IndexOutOfBoundsException e) {
            LOGGER.warn(String.format("Step: %d not found in test: %s", step, testName));
            return;
        }

        try {
            jiraManager.updateStepResult(executionStep, status, comment);
        } catch (JiraZephyrException e) {
            LOGGER.warn("Could not update JIRA execution step due to error.", e);
        }

        if (status != null) lastSuccessfulStep = step; // If we only want to add a comment, to fail the test afterwards we don't want to make it think this step is over
    }

    public void failTest() {
        try {
            updateStepResult(lastSuccessfulStep + 1, TestStatus.FAIL, null); // If we have a meaningful comment, it should have been added by the test itself already
        } catch (IndexOutOfBoundsException ignored) {} // If we already marked the last step, there is no next step (think pipeline tests that fail the whole test after all steps are done)

        updateExecutionStatus(TestStatus.FAIL);
    }

    public void passTest() {
        updateExecutionStatus(TestStatus.PASS);
    }

    public void skipTest() {
        updateExecutionStatus(TestStatus.BLOCKED);
    }

    private TestStep getNaturalStep(int step) throws IndexOutOfBoundsException {
        return testSteps.get(step - 1); // natural ordering of test steps 1, 2, 3, ... to match what is returned by ZAPI
    }

}