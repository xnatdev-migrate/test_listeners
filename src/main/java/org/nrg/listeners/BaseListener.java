package org.nrg.listeners;

import org.nrg.util.TestNgUtils;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public abstract class BaseListener extends TestListenerAdapter {

    protected Class testClass = null;
    protected String testClassName = null;
    protected String testName = null;

    @Override
    public void onTestStart(ITestResult result) {
        super.onTestStart(result);
        extractSimpleTestInfo(result);
    }

    @Override
    public void onTestFailure(ITestResult testResult) {
        super.onTestFailure(testResult);
        extractSimpleTestInfo(testResult);
        onTestComplete(testResult);
    }

    @Override
    public void onTestSuccess(ITestResult testResult) {
        super.onTestSuccess(testResult);
        extractSimpleTestInfo(testResult);
        onTestComplete(testResult);
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        super.onTestSkipped(testResult);
        extractSimpleTestInfo(testResult);
        onTestComplete(testResult);
    }

    /**
     * Should run whenever a test completes (through one of onTestFailure(), onTestSuccess(), or onTestSkipped()
     * @param testResult Test having finished
     */
    public abstract void onTestComplete(ITestResult testResult);


    protected void extractSimpleTestInfo(ITestResult testResult) {
        testClass = TestNgUtils.getTestClass(testResult);
        testClassName = testClass.getSimpleName();
        testName = TestNgUtils.getTestName(testResult);
    }

}
